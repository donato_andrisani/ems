package com.thinkopen.emmelibri.service;

import com.thinkopen.emmelibri.model.User;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;


public class UserServiceImplTest {

    @Test
    public void testFindAllUsers() throws Exception {
        UserServiceImpl userService = new UserServiceImpl();
        List<User> allUsers = userService.findAllUsers();
        Assert.assertNotNull(allUsers);
    }
}
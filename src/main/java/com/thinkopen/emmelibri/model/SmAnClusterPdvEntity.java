package com.thinkopen.emmelibri.model;

import javax.persistence.*;

/**
 * Created by donato on 9/19/17.
 */
@Entity
@Table(name = "sm_an_cluster_pdv", schema = "opty_sellout", catalog = "EXA_DB")
@IdClass(SmAnClusterPdvEntityPK.class)
public class SmAnClusterPdvEntity {
    private String catena;
    private String cluster;
    private Integer capienza;

    @Id
    @Column(name = "catena", nullable = false, insertable = true, updatable = true, length = 100)
    public String getCatena() {
        return catena;
    }

    public void setCatena(String catena) {
        this.catena = catena;
    }

    @Id
    @Column(name = "cluster", nullable = false, insertable = true, updatable = true, length = 1)
    public String getCluster() {
        return cluster;
    }

    public void setCluster(String cluster) {
        this.cluster = cluster;
    }

    @Basic
    @Column(name = "capienza", nullable = true, insertable = true, updatable = true, precision = 0)
    public Integer getCapienza() {
        return capienza;
    }

    public void setCapienza(Integer capienza) {
        this.capienza = capienza;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SmAnClusterPdvEntity that = (SmAnClusterPdvEntity) o;

        if (capienza != null ? !capienza.equals(that.capienza) : that.capienza != null) return false;
        if (catena != null ? !catena.equals(that.catena) : that.catena != null) return false;
        if (cluster != null ? !cluster.equals(that.cluster) : that.cluster != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = catena != null ? catena.hashCode() : 0;
        result = 31 * result + (cluster != null ? cluster.hashCode() : 0);
        result = 31 * result + (capienza != null ? capienza.hashCode() : 0);
        return result;
    }
}

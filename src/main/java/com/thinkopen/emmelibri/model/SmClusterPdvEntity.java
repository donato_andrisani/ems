package com.thinkopen.emmelibri.model;

import javax.persistence.*;

/**
 * Created by donato on 9/19/17.
 */
@Entity
@Table(name = "sm_cluster_pdv", schema = "opty_sellout", catalog = "EXA_DB")
public class SmClusterPdvEntity {
    private String catena;
    private String codicepdv;
    private String cluster;
    private Integer riordino;
    private Integer sottoscorta;

    @Basic
    @Column(name = "catena", nullable = true, insertable = true, updatable = true, length = 100)
    public String getCatena() {
        return catena;
    }

    public void setCatena(String catena) {
        this.catena = catena;
    }

    @Id
    @Column(name = "codicepdv", nullable = false, insertable = true, updatable = true, length = 10)
    public String getCodicepdv() {
        return codicepdv;
    }

    public void setCodicepdv(String codicepdv) {
        this.codicepdv = codicepdv;
    }

    @Basic
    @Column(name = "cluster", nullable = true, insertable = true, updatable = true, length = 1)
    public String getCluster() {
        return cluster;
    }

    public void setCluster(String cluster) {
        this.cluster = cluster;
    }

    @Basic
    @Column(name = "riordino", nullable = true, insertable = true, updatable = true, precision = 0)
    public Integer getRiordino() {
        return riordino;
    }

    public void setRiordino(Integer riordino) {
        this.riordino = riordino;
    }

    @Basic
    @Column(name = "sottoscorta", nullable = true, insertable = true, updatable = true, precision = 0)
    public Integer getSottoscorta() {
        return sottoscorta;
    }

    public void setSottoscorta(Integer sottoscorta) {
        this.sottoscorta = sottoscorta;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SmClusterPdvEntity that = (SmClusterPdvEntity) o;

        if (catena != null ? !catena.equals(that.catena) : that.catena != null) return false;
        if (cluster != null ? !cluster.equals(that.cluster) : that.cluster != null) return false;
        if (codicepdv != null ? !codicepdv.equals(that.codicepdv) : that.codicepdv != null) return false;
        if (riordino != null ? !riordino.equals(that.riordino) : that.riordino != null) return false;
        if (sottoscorta != null ? !sottoscorta.equals(that.sottoscorta) : that.sottoscorta != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = catena != null ? catena.hashCode() : 0;
        result = 31 * result + (codicepdv != null ? codicepdv.hashCode() : 0);
        result = 31 * result + (cluster != null ? cluster.hashCode() : 0);
        result = 31 * result + (riordino != null ? riordino.hashCode() : 0);
        result = 31 * result + (sottoscorta != null ? sottoscorta.hashCode() : 0);
        return result;
    }
}

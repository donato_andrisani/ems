package com.thinkopen.emmelibri.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.sql.Date;

/**
 * Created by donato on 9/19/17.
 */
@Entity
@Table(name = "ta_sellout_mediaworld", schema = "opty_sellout", catalog = "EXA_DB")
public class TaSelloutMediaworldEntity {
    private Date dDataMovimento;
    private String kProdotto;
    private String kCliente;
    private String kFornitore;
    private BigDecimal iFasciaPrezzo;
    private BigDecimal iPrezzoCopertina;
    private BigDecimal iValoreLordo;
    private BigDecimal iValoreNetto;
    private Integer qQuantita;
    private String kCausale;
    private String kMagazzino;
    private String flussoProvenienza;
    private Date dDataInserimento;
    private String nFile;

    @Basic
    @Column(name = "d_data_movimento", nullable = true, insertable = true, updatable = true)
    public Date getdDataMovimento() {
        return dDataMovimento;
    }

    public void setdDataMovimento(Date dDataMovimento) {
        this.dDataMovimento = dDataMovimento;
    }

    @Basic
    @Column(name = "k_prodotto", nullable = true, insertable = true, updatable = true, length = 15)
    public String getkProdotto() {
        return kProdotto;
    }

    public void setkProdotto(String kProdotto) {
        this.kProdotto = kProdotto;
    }

    @Basic
    @Column(name = "k_cliente", nullable = true, insertable = true, updatable = true, length = 6)
    public String getkCliente() {
        return kCliente;
    }

    public void setkCliente(String kCliente) {
        this.kCliente = kCliente;
    }

    @Basic
    @Column(name = "k_fornitore", nullable = true, insertable = true, updatable = true, length = 6)
    public String getkFornitore() {
        return kFornitore;
    }

    public void setkFornitore(String kFornitore) {
        this.kFornitore = kFornitore;
    }

    @Basic
    @Column(name = "i_fascia_prezzo", nullable = true, insertable = true, updatable = true, precision = 2)
    public BigDecimal getiFasciaPrezzo() {
        return iFasciaPrezzo;
    }

    public void setiFasciaPrezzo(BigDecimal iFasciaPrezzo) {
        this.iFasciaPrezzo = iFasciaPrezzo;
    }

    @Basic
    @Column(name = "i_prezzo_copertina", nullable = true, insertable = true, updatable = true, precision = 2)
    public BigDecimal getiPrezzoCopertina() {
        return iPrezzoCopertina;
    }

    public void setiPrezzoCopertina(BigDecimal iPrezzoCopertina) {
        this.iPrezzoCopertina = iPrezzoCopertina;
    }

    @Basic
    @Column(name = "i_valore_lordo", nullable = true, insertable = true, updatable = true, precision = 2)
    public BigDecimal getiValoreLordo() {
        return iValoreLordo;
    }

    public void setiValoreLordo(BigDecimal iValoreLordo) {
        this.iValoreLordo = iValoreLordo;
    }

    @Basic
    @Column(name = "i_valore_netto", nullable = true, insertable = true, updatable = true, precision = 2)
    public BigDecimal getiValoreNetto() {
        return iValoreNetto;
    }

    public void setiValoreNetto(BigDecimal iValoreNetto) {
        this.iValoreNetto = iValoreNetto;
    }

    @Basic
    @Column(name = "q_quantita", nullable = true, insertable = true, updatable = true, precision = 0)
    public Integer getqQuantita() {
        return qQuantita;
    }

    public void setqQuantita(Integer qQuantita) {
        this.qQuantita = qQuantita;
    }

    @Basic
    @Column(name = "k_causale", nullable = true, insertable = true, updatable = true, length = 3)
    public String getkCausale() {
        return kCausale;
    }

    public void setkCausale(String kCausale) {
        this.kCausale = kCausale;
    }

    @Basic
    @Column(name = "k_magazzino", nullable = true, insertable = true, updatable = true, length = 2)
    public String getkMagazzino() {
        return kMagazzino;
    }

    public void setkMagazzino(String kMagazzino) {
        this.kMagazzino = kMagazzino;
    }

    @Basic
    @Column(name = "flusso_provenienza", nullable = true, insertable = true, updatable = true, length = 30)
    public String getFlussoProvenienza() {
        return flussoProvenienza;
    }

    public void setFlussoProvenienza(String flussoProvenienza) {
        this.flussoProvenienza = flussoProvenienza;
    }

    @Basic
    @Column(name = "d_data_inserimento", nullable = true, insertable = true, updatable = true)
    public Date getdDataInserimento() {
        return dDataInserimento;
    }

    public void setdDataInserimento(Date dDataInserimento) {
        this.dDataInserimento = dDataInserimento;
    }

    @Basic
    @Column(name = "n_file", nullable = true, insertable = true, updatable = true, length = 255)
    public String getnFile() {
        return nFile;
    }

    public void setnFile(String nFile) {
        this.nFile = nFile;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TaSelloutMediaworldEntity that = (TaSelloutMediaworldEntity) o;

        if (dDataInserimento != null ? !dDataInserimento.equals(that.dDataInserimento) : that.dDataInserimento != null)
            return false;
        if (dDataMovimento != null ? !dDataMovimento.equals(that.dDataMovimento) : that.dDataMovimento != null)
            return false;
        if (flussoProvenienza != null ? !flussoProvenienza.equals(that.flussoProvenienza) : that.flussoProvenienza != null)
            return false;
        if (iFasciaPrezzo != null ? !iFasciaPrezzo.equals(that.iFasciaPrezzo) : that.iFasciaPrezzo != null)
            return false;
        if (iPrezzoCopertina != null ? !iPrezzoCopertina.equals(that.iPrezzoCopertina) : that.iPrezzoCopertina != null)
            return false;
        if (iValoreLordo != null ? !iValoreLordo.equals(that.iValoreLordo) : that.iValoreLordo != null) return false;
        if (iValoreNetto != null ? !iValoreNetto.equals(that.iValoreNetto) : that.iValoreNetto != null) return false;
        if (kCausale != null ? !kCausale.equals(that.kCausale) : that.kCausale != null) return false;
        if (kCliente != null ? !kCliente.equals(that.kCliente) : that.kCliente != null) return false;
        if (kFornitore != null ? !kFornitore.equals(that.kFornitore) : that.kFornitore != null) return false;
        if (kMagazzino != null ? !kMagazzino.equals(that.kMagazzino) : that.kMagazzino != null) return false;
        if (kProdotto != null ? !kProdotto.equals(that.kProdotto) : that.kProdotto != null) return false;
        if (nFile != null ? !nFile.equals(that.nFile) : that.nFile != null) return false;
        if (qQuantita != null ? !qQuantita.equals(that.qQuantita) : that.qQuantita != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = dDataMovimento != null ? dDataMovimento.hashCode() : 0;
        result = 31 * result + (kProdotto != null ? kProdotto.hashCode() : 0);
        result = 31 * result + (kCliente != null ? kCliente.hashCode() : 0);
        result = 31 * result + (kFornitore != null ? kFornitore.hashCode() : 0);
        result = 31 * result + (iFasciaPrezzo != null ? iFasciaPrezzo.hashCode() : 0);
        result = 31 * result + (iPrezzoCopertina != null ? iPrezzoCopertina.hashCode() : 0);
        result = 31 * result + (iValoreLordo != null ? iValoreLordo.hashCode() : 0);
        result = 31 * result + (iValoreNetto != null ? iValoreNetto.hashCode() : 0);
        result = 31 * result + (qQuantita != null ? qQuantita.hashCode() : 0);
        result = 31 * result + (kCausale != null ? kCausale.hashCode() : 0);
        result = 31 * result + (kMagazzino != null ? kMagazzino.hashCode() : 0);
        result = 31 * result + (flussoProvenienza != null ? flussoProvenienza.hashCode() : 0);
        result = 31 * result + (dDataInserimento != null ? dDataInserimento.hashCode() : 0);
        result = 31 * result + (nFile != null ? nFile.hashCode() : 0);
        return result;
    }
}

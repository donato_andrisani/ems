package com.thinkopen.emmelibri.model;

import javax.persistence.*;
import java.math.BigInteger;

/**
 * Created by donato on 9/19/17.
 */
@Entity
@Table(name = "sm_eccezioni", schema = "opty_sellout", catalog = "EXA_DB")
@IdClass(SmEccezioniEntityPK.class)
public class SmEccezioniEntity {
    private String catena;
    private String ean;
    private int anno;
    private BigInteger settimana;

    @Id
    @Column(name = "catena", nullable = false, insertable = true, updatable = true, length = 100)
    public String getCatena() {
        return catena;
    }

    public void setCatena(String catena) {
        this.catena = catena;
    }

    @Id
    @Column(name = "ean", nullable = false, insertable = true, updatable = true, length = 13)
    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    @Id
    @Column(name = "anno", nullable = false, insertable = true, updatable = true, precision = 0)
    public int getAnno() {
        return anno;
    }

    public void setAnno(int anno) {
        this.anno = anno;
    }

    @Id
    @Column(name = "settimana", nullable = false, insertable = true, updatable = true, precision = 0)
    public BigInteger getSettimana() {
        return settimana;
    }

    public void setSettimana(BigInteger settimana) {
        this.settimana = settimana;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SmEccezioniEntity that = (SmEccezioniEntity) o;

        if (anno != that.anno) return false;
        if (catena != null ? !catena.equals(that.catena) : that.catena != null) return false;
        if (ean != null ? !ean.equals(that.ean) : that.ean != null) return false;
        if (settimana != null ? !settimana.equals(that.settimana) : that.settimana != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = catena != null ? catena.hashCode() : 0;
        result = 31 * result + (ean != null ? ean.hashCode() : 0);
        result = 31 * result + anno;
        result = 31 * result + (settimana != null ? settimana.hashCode() : 0);
        return result;
    }
}

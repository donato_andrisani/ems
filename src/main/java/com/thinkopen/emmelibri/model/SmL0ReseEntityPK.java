package com.thinkopen.emmelibri.model;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by donato on 9/19/17.
 */
public class SmL0ReseEntityPK implements Serializable {
    private String codicepdv;

    @Column(name = "codicepdv", nullable = false, insertable = true, updatable = true, length = 10)
    @Id
    public String getCodicepdv() {
        return codicepdv;
    }

    public void setCodicepdv(String codicepdv) {
        this.codicepdv = codicepdv;
    }

    private String ean;

    @Column(name = "ean", nullable = false, insertable = true, updatable = true, length = 13)
    @Id
    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    private int qta;

    @Column(name = "qta", nullable = false, insertable = true, updatable = true, precision = 0)
    @Id
    public int getQta() {
        return qta;
    }

    public void setQta(int qta) {
        this.qta = qta;
    }

    private String dataResa;

    @Column(name = "data_resa", nullable = false, insertable = true, updatable = true, length = 8)
    @Id
    public String getDataResa() {
        return dataResa;
    }

    public void setDataResa(String dataResa) {
        this.dataResa = dataResa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SmL0ReseEntityPK that = (SmL0ReseEntityPK) o;

        if (qta != that.qta) return false;
        if (codicepdv != null ? !codicepdv.equals(that.codicepdv) : that.codicepdv != null) return false;
        if (dataResa != null ? !dataResa.equals(that.dataResa) : that.dataResa != null) return false;
        if (ean != null ? !ean.equals(that.ean) : that.ean != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = codicepdv != null ? codicepdv.hashCode() : 0;
        result = 31 * result + (ean != null ? ean.hashCode() : 0);
        result = 31 * result + qta;
        result = 31 * result + (dataResa != null ? dataResa.hashCode() : 0);
        return result;
    }
}

package com.thinkopen.emmelibri.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by donato on 9/19/17.
 */
@Entity
@Table(name = "sm_gia_opty", schema = "opty_sellout", catalog = "EXA_DB")
public class SmGiaOptyEntity {
    private String ean;
    private Integer qta;
    private String dataGiacenza;

    @Basic
    @Column(name = "ean", nullable = true, insertable = true, updatable = true, length = 13)
    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    @Basic
    @Column(name = "qta", nullable = true, insertable = true, updatable = true, precision = 0)
    public Integer getQta() {
        return qta;
    }

    public void setQta(Integer qta) {
        this.qta = qta;
    }

    @Basic
    @Column(name = "data_giacenza", nullable = true, insertable = true, updatable = true, length = 8)
    public String getDataGiacenza() {
        return dataGiacenza;
    }

    public void setDataGiacenza(String dataGiacenza) {
        this.dataGiacenza = dataGiacenza;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SmGiaOptyEntity that = (SmGiaOptyEntity) o;

        if (dataGiacenza != null ? !dataGiacenza.equals(that.dataGiacenza) : that.dataGiacenza != null) return false;
        if (ean != null ? !ean.equals(that.ean) : that.ean != null) return false;
        if (qta != null ? !qta.equals(that.qta) : that.qta != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = ean != null ? ean.hashCode() : 0;
        result = 31 * result + (qta != null ? qta.hashCode() : 0);
        result = 31 * result + (dataGiacenza != null ? dataGiacenza.hashCode() : 0);
        return result;
    }
}

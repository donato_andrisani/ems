package com.thinkopen.emmelibri.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by donato on 9/19/17.
 */
@Entity
@Table(name = "sm_acq_opty", schema = "opty_sellout", catalog = "EXA_DB")
public class SmAcqOptyEntity {
    private String ean;
    private Integer qta;
    private String dataAcquisto;

    @Basic
    @Column(name = "ean", nullable = true, insertable = true, updatable = true, length = 13)
    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    @Basic
    @Column(name = "qta", nullable = true, insertable = true, updatable = true, precision = 0)
    public Integer getQta() {
        return qta;
    }

    public void setQta(Integer qta) {
        this.qta = qta;
    }

    @Basic
    @Column(name = "data_acquisto", nullable = true, insertable = true, updatable = true, length = 8)
    public String getDataAcquisto() {
        return dataAcquisto;
    }

    public void setDataAcquisto(String dataAcquisto) {
        this.dataAcquisto = dataAcquisto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SmAcqOptyEntity that = (SmAcqOptyEntity) o;

        if (dataAcquisto != null ? !dataAcquisto.equals(that.dataAcquisto) : that.dataAcquisto != null) return false;
        if (ean != null ? !ean.equals(that.ean) : that.ean != null) return false;
        if (qta != null ? !qta.equals(that.qta) : that.qta != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = ean != null ? ean.hashCode() : 0;
        result = 31 * result + (qta != null ? qta.hashCode() : 0);
        result = 31 * result + (dataAcquisto != null ? dataAcquisto.hashCode() : 0);
        return result;
    }
}

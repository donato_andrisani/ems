package com.thinkopen.emmelibri.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by donato on 9/19/17.
 */
@Entity
@Table(name = "sm_vend_opty", schema = "opty_sellout", catalog = "EXA_DB")
public class SmVendOptyEntity {
    private String ean;
    private Integer qta;
    private String dataVenduto;

    @Basic
    @Column(name = "ean", nullable = true, insertable = true, updatable = true, length = 13)
    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    @Basic
    @Column(name = "qta", nullable = true, insertable = true, updatable = true, precision = 0)
    public Integer getQta() {
        return qta;
    }

    public void setQta(Integer qta) {
        this.qta = qta;
    }

    @Basic
    @Column(name = "data_venduto", nullable = true, insertable = true, updatable = true, length = 8)
    public String getDataVenduto() {
        return dataVenduto;
    }

    public void setDataVenduto(String dataVenduto) {
        this.dataVenduto = dataVenduto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SmVendOptyEntity that = (SmVendOptyEntity) o;

        if (dataVenduto != null ? !dataVenduto.equals(that.dataVenduto) : that.dataVenduto != null) return false;
        if (ean != null ? !ean.equals(that.ean) : that.ean != null) return false;
        if (qta != null ? !qta.equals(that.qta) : that.qta != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = ean != null ? ean.hashCode() : 0;
        result = 31 * result + (qta != null ? qta.hashCode() : 0);
        result = 31 * result + (dataVenduto != null ? dataVenduto.hashCode() : 0);
        return result;
    }
}

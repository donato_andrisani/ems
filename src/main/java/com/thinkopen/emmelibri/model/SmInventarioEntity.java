package com.thinkopen.emmelibri.model;

import javax.persistence.*;
import java.math.BigInteger;

/**
 * Created by donato on 9/19/17.
 */
@Entity
@Table(name = "sm_inventario", schema = "opty_sellout", catalog = "EXA_DB")
@IdClass(SmInventarioEntityPK.class)
public class SmInventarioEntity {
    private BigInteger tpmov;
    private String catena;
    private String codicepdv;
    private String dataInventario;
    private String ean;
    private Integer qta;

    @Id
    @Column(name = "tpmov", nullable = false, insertable = true, updatable = true, precision = 0)
    public BigInteger getTpmov() {
        return tpmov;
    }

    public void setTpmov(BigInteger tpmov) {
        this.tpmov = tpmov;
    }

    @Basic
    @Column(name = "catena", nullable = true, insertable = true, updatable = true, length = 100)
    public String getCatena() {
        return catena;
    }

    public void setCatena(String catena) {
        this.catena = catena;
    }

    @Id
    @Column(name = "codicepdv", nullable = false, insertable = true, updatable = true, length = 10)
    public String getCodicepdv() {
        return codicepdv;
    }

    public void setCodicepdv(String codicepdv) {
        this.codicepdv = codicepdv;
    }

    @Basic
    @Column(name = "data_inventario", nullable = true, insertable = true, updatable = true, length = 8)
    public String getDataInventario() {
        return dataInventario;
    }

    public void setDataInventario(String dataInventario) {
        this.dataInventario = dataInventario;
    }

    @Id
    @Column(name = "ean", nullable = false, insertable = true, updatable = true, length = 13)
    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    @Basic
    @Column(name = "qta", nullable = true, insertable = true, updatable = true, precision = 0)
    public Integer getQta() {
        return qta;
    }

    public void setQta(Integer qta) {
        this.qta = qta;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SmInventarioEntity that = (SmInventarioEntity) o;

        if (catena != null ? !catena.equals(that.catena) : that.catena != null) return false;
        if (codicepdv != null ? !codicepdv.equals(that.codicepdv) : that.codicepdv != null) return false;
        if (dataInventario != null ? !dataInventario.equals(that.dataInventario) : that.dataInventario != null)
            return false;
        if (ean != null ? !ean.equals(that.ean) : that.ean != null) return false;
        if (qta != null ? !qta.equals(that.qta) : that.qta != null) return false;
        if (tpmov != null ? !tpmov.equals(that.tpmov) : that.tpmov != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = tpmov != null ? tpmov.hashCode() : 0;
        result = 31 * result + (catena != null ? catena.hashCode() : 0);
        result = 31 * result + (codicepdv != null ? codicepdv.hashCode() : 0);
        result = 31 * result + (dataInventario != null ? dataInventario.hashCode() : 0);
        result = 31 * result + (ean != null ? ean.hashCode() : 0);
        result = 31 * result + (qta != null ? qta.hashCode() : 0);
        return result;
    }
}

package com.thinkopen.emmelibri.model;

import javax.persistence.*;

/**
 * Created by donato on 9/19/17.
 */
@Entity
@Table(name = "sm_incidenze", schema = "opty_sellout", catalog = "EXA_DB")
@IdClass(SmIncidenzeEntityPK.class)
public class SmIncidenzeEntity {
    private String catena;
    private String cluster;
    private String genere;
    private Integer allocazione;

    @Id
    @Column(name = "catena", nullable = false, insertable = true, updatable = true, length = 100)
    public String getCatena() {
        return catena;
    }

    public void setCatena(String catena) {
        this.catena = catena;
    }

    @Id
    @Column(name = "cluster", nullable = false, insertable = true, updatable = true, length = 13)
    public String getCluster() {
        return cluster;
    }

    public void setCluster(String cluster) {
        this.cluster = cluster;
    }

    @Id
    @Column(name = "genere", nullable = false, insertable = true, updatable = true, length = 100)
    public String getGenere() {
        return genere;
    }

    public void setGenere(String genere) {
        this.genere = genere;
    }

    @Basic
    @Column(name = "allocazione", nullable = true, insertable = true, updatable = true, precision = 0)
    public Integer getAllocazione() {
        return allocazione;
    }

    public void setAllocazione(Integer allocazione) {
        this.allocazione = allocazione;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SmIncidenzeEntity that = (SmIncidenzeEntity) o;

        if (allocazione != null ? !allocazione.equals(that.allocazione) : that.allocazione != null) return false;
        if (catena != null ? !catena.equals(that.catena) : that.catena != null) return false;
        if (cluster != null ? !cluster.equals(that.cluster) : that.cluster != null) return false;
        if (genere != null ? !genere.equals(that.genere) : that.genere != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = catena != null ? catena.hashCode() : 0;
        result = 31 * result + (cluster != null ? cluster.hashCode() : 0);
        result = 31 * result + (genere != null ? genere.hashCode() : 0);
        result = 31 * result + (allocazione != null ? allocazione.hashCode() : 0);
        return result;
    }
}

package com.thinkopen.emmelibri;

import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;



@Configuration
public class HibernateConfiguration {

    private SessionFactory _sessionFactory;


    @Bean(name = "sessionFactory")
    public SessionFactory getSessionFactory() {
        if (_sessionFactory == null) {
            _sessionFactory = createSessionFactory();
        }
        return _sessionFactory;
    }

    private SessionFactory createSessionFactory() {
        org.hibernate.cfg.Configuration _conf = new org.hibernate.cfg.Configuration().configure();
        return _conf.buildSessionFactory();

    }


}
